import static org.junit.Assert.*;
import org.junit.*;
import java.util.*;

public class OddOrPosTest
{
   /**
    * Solution
    */
   @Test
   public void test ()
   {
       int arr[] = {-3, -2, 0, 1, 3};
       assertEquals("zeros!!!", 3, OddOrPos.oddOrPos(arr));
   }
}
