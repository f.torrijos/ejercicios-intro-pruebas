import java.util.*;
import java.io.*;
import java.util.Scanner;

public class OddOrPos
{
    /**
     * Count odd or positive elements in an array
     *
     * @param x array to search
     * @return count of odd or positive elements in x
     * @throws NullPointerException if x is null
     */
   public static int oddOrPos (int[] x)
   {  
      int count = 0;
   
      for (int i = 0; i < x.length; i++)
      {
         // FAIL: 0
         // if (x[i]%2 == 1 || x[i] > 0) // ERROR
         if (x[i]%2 == 1 || x[i] >= 0) // SOLUTION
         {
            count++;
         }
      }
      return count;
   } 

   public static void main (String argv[])
   {  
      int in[] = new int[argv.length];
      int element;
      int i;

      if (argv.length == 0)
      {
         System.out.println ("usage: java OddOrPos n1 n2 n3 ... ");
         return;
      }

      for (i=0; i<argv.length; i++)
      {
         try
         {
            in[i] = Integer.parseInt (argv[i]);
         }
         catch (NumberFormatException e)
         {
            System.out.println ("introduce a number");
         }
      }

      System.out.println ("elements odd or positive: " + oddOrPos (in));
   }
}
