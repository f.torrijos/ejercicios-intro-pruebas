//
// ---> 'assert': 'must be'
//

import static org.junit.Assert.*;
import org.junit.*;
import java.util.*;

public class CountPositiveTest
{
   private int arr[]; 

   @Before
   public void setUp ()
   {
      arr = new int[]{0, 1, 2};
   }

   @After
   public void tearDown ()
   {
      arr = null;
   }

   @Test
   public void testForNullArray ()
   {
     arr = null;
     try
     {
        CountPositive.countPositive (arr);
     }
     catch (NullPointerException e)
     {
        return;
     } 
     fail ("NullPointerException expected");
   }

   @Test
   public void testSingleElement()
   {
      int arr[] = {1};
      Object obj = CountPositive.countPositive (arr);
      assertTrue ("single element", obj.equals (1));
   }

   /** 
    * Solution 
    */
   @Test
   public void testZerosInArray ()
   {
       int arr[] = {0, 3, -1, 5, 7, 2};
       assertEquals ("zeroes", 4, CountPositive.countPositive(arr));
   }
}
