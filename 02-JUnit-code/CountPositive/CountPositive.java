import java.util.*;
import java.io.*;

public class CountPositive
{
   /** 
   * Counts positive elements in array
   *
   * @param x array to search
   * @return number of positive elements in x
   * @throws NullPointerException if x is null
   */
   public static int countPositive (int[] x)
   {
      int count = 0;
   
      for (int i=0; i < x.length; i++)
      {
         // FAIL: 0
         // ERROR: if (x[i] >= 0)
         if (x[i] > 0) // SOLUTION
         {
            count++;
         }
      }
      return count;
   }

   public static void main (String argv[])
   {  
      int in[] = new int[argv.length];
      int i;

      if (argv.length == 0)
      {
         System.out.println ("usage: java CountPositive n1 n2 n3 ... ");
         return;
      }

      for (i=0; i<argv.length; i++)
      {
         try
         {
            in[i] = Integer.parseInt (argv[i]);
         }
         catch (NumberFormatException e)
         {
            System.out.println ("introduce a number");
         }
      }

      System.out.println ("number of positive numbers is: " + countPositive (in));
   }
}
