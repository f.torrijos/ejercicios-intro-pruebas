import java.util.*;
import java.io.*;
import java.util.Scanner;

public class FindLast
{
   /**
    * Find last index of element
    * 
    *  @param x array to search
    *  @param y value to look for
    *  @return last index of y in x; -1 if absent
    *  @throws NullPointerException if x is null
    */
   public static int findLast (int[] x, int y)
   { 
      // FAIL: 0
      // for (int i=x.length-1; i > 0; i--) // ERROR
      for (int i=x.length-1; i >=0; i--) // SOLUTION
      {
         if (x[i] == y) 
         {
            return i;
         }
      }
      return -1;
   }
   
   public static void main (String argv[])
   {  
      int in[] = new int[argv.length];
      int element;
      int i;

      if (argv.length == 0)
      {
         System.out.println ("usage: java FindLast n1 n2 n3 ... ");
         return;
      }

      for (i=0; i<argv.length; i++)
      {
         try
         {
            in[i] = Integer.parseInt (argv[i]);
         }
         catch (NumberFormatException e)
         {
            System.out.println ("introduces numbers");
         }
      }

      // write 'stdin'
      Scanner keyboard = new Scanner(System.in);
      System.out.print ("introduce element: ");
      element = keyboard.nextInt();

      System.out.println ("last index of element " + findLast (in, element));
   }
}
