import static org.junit.Assert.*;
import org.junit.*;
import java.util.*;

public class FindLastTest
{
   /**
    * Solution
    */
   @Test
   public void testSeveralZerosInArray ()
   {
       int arr[] = {1, 2, 3, 4, 5};
       int y = 1;
       assertEquals("if last position is in first element", 0, FindLast.findLast(arr, y));
   }
}
