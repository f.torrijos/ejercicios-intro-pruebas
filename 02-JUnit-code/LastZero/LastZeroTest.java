import static org.junit.Assert.*;
import org.junit.*;
import java.util.*;

public class LastZeroTest
{
   /**
    * Solution
    */
   @Test
   public void testSeveralZerosInArray ()
   {
       int arr[] = {0, 3, 2, 1, 0};
       assertEquals("zeroes", 4, LastZero.lastZero(arr));
   }
}
