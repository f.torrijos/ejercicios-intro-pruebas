import java.util.*;
import java.io.*;

public class LastZero
{
   /**
    * Find LAST index of zero
    *
    * @param x array to search
    * @return index of last 0 in x; -1 if absent
    * @throws NullPointerException if x is null
    */
   public static int lastZero (int[] x)
   {
      // FAIL: start at the beginning
      // ERROR: for (int i = 0; i < x.length; i++)
      for (int i = x.length-1; i >= -1; i--) // SOLUTION
      {
         if (x[i] == 0)
         {
            return i;
         }
      }
      return -1;
   }

   public static void main (String argv[])
   {  
      int in[] = new int[argv.length];
      int i;

      if (argv.length == 0)
      {
         System.out.println ("usage: java LastZero n1 n2 n3 ... ");
         return;
      }

      for (i=0; i<argv.length; i++)
      {
         try
         {
            in[i] = Integer.parseInt (argv[i]);
         }
         catch (NumberFormatException e)
         {
            System.out.println ("introduce a number");
         }
      }

      System.out.println ("last index of zero is: " + lastZero (in));
   }
}
